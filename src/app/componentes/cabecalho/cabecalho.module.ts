import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {MenuModule} from '../menu/menu.module';
import {CabecalhoComponent} from './cabecalho.component';

@NgModule({
  declarations: [CabecalhoComponent],
  imports: [CommonModule, RouterModule, MenuModule, SharedModule],
  exports: [CabecalhoComponent]
})
export class CabecalhoModule {}
