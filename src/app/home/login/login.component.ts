import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AutenticacaoService} from '../../autenticacao/autenticacao.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  usuario = '';
  senha = '';

  constructor(
    private authService: AutenticacaoService,
    private router: Router
  ) {}

  login(): void {
    this.authService.autenticar(this.usuario, this.senha).subscribe(() =>
      this.router.navigate(['animais']).catch((error) => {
        alert('Usuario ou senha inválidos!');
        console.log(error);
      })
    );
  }
}
