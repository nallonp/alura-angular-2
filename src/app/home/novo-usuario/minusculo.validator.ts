import {AbstractControl} from '@angular/forms';

export function minusculoValidator(control: AbstractControl) {
  /* return control.valueChanges
    .pipe(debounceTime(500))
    .pipe(
      map((username) =>
        username !== username.toLowerCase() ? {minusculo: true} : null
      )
    )
    .pipe(first());*/
  const valor = control.value as string;
  return valor !== valor.toLowerCase() ? {minusculo: true} : null;
}
