import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {NovoUsuario} from './novo-usuario';

const API = environment.apiURL;

@Injectable({
  providedIn: 'root'
})
export class NovoUsuarioService {
  constructor(private httpClient: HttpClient) {}

  cadastraNovoUsuarino(novoUsuario: NovoUsuario): Observable<any> {
    return this.httpClient.post<NovoUsuario>(`${API}/user/signup`, novoUsuario);
  }
  verificaUsuarioExistente(nomeUsuario: string): Observable<boolean> {
    return this.httpClient.get(
      `${API}/user/exists/${nomeUsuario}`
    ) as Observable<boolean>;
  }
}
