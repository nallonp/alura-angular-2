import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {AnimaisModule} from './animais/animais.module';
import {NovoAnimalComponent} from './animais/novo-animal/novo-animal.component';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AutenticacaoModule} from './autenticacao/autenticacao.module';
import {CabecalhoModule} from './componentes/cabecalho/cabecalho.module';
import {RodapeModule} from './componentes/rodape/rodape.module';
import {SharedModule} from './shared/shared.module';

@NgModule({
  declarations: [AppComponent, NovoAnimalComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CabecalhoModule,
    RodapeModule,
    AutenticacaoModule,
    ReactiveFormsModule,
    SharedModule,
    AnimaisModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
